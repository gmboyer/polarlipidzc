# Polar Lipid ZC
### Supplementary Code and Data for Boyer et al. 2020

This repository contains data and code used in [Boyer et al. 2020](https://doi.org/10.3389/fmicb.2020.00229) (DOI:10.3389/fmicb.2020.00229). Data are included in an Excel workbook that is accessed by R custom scripts for processing, calculations, and visualizations. Code for reproducing Figures 4 through 7 is also included.

### Prerequisites

You will need R installed on your machine. R version 3.5.3 was used at the time these scripts were created. You will also need the following R libraries. The number after the library name indicates the version we used.

- msir 1.3.2
- ggpubr 0.2
- magrittr 1.5
- quantreg 5.38
- SparseM 1.77
- ggthemes 4.1.1
- forcats 0.4.0
- stringr 1.4.0
- dplyr 0.8.0.1
- purrr 0.3.2
- readr 1.3.1
- tidyr 0.8.3
- tibble 2.1.1
- tidyverse 1.2.1
- reshape2 1.4.3
- reshape 0.8.8
- CHNOSZ 1.3.1
- readxl 1.3.1
- scales 1.0.0

Install packages in RStudio or the R console with ```install.packages("package_name")```. For instance:

```
install.packages("CHNOSZ")
```

A specific version of a package can be installed from the CRAN archive in case a newer version doesn't work. For instance, installing version 1.3.1 of the CHNOSZ package can be performed with:

```
url <- "https://cran.r-project.org/src/contrib/Archive/CHNOSZ/CHNOSZ_1.3.1.tar.gz"
install.packages(url, repos=NULL, type="source")
```

We recommend installing Jupyter Notebooks for running the scripts in this repository. We included a Jupyter notebook to efficiently execute all scripts in order. [Instructions for installing Jupyter notebooks can be found here](https://jupyter.org/install.html). We used Jupyter Notebooks version 5.7.3 at the time this repository was created. For Jupyter to be able to run R scripts, it needs an R kernel. An R kernel can be added to Jupyter notebooks using the R package 'IRkernel' and following the two step process shown [here](https://irkernel.github.io/installation/).

You can install the prerequisite R libraries within a Jupyter Notebook cell by running with an R kernel by using the same command shown above.

### Installing

Click on the 'download' button and select 'download as ZIP' in the GitLab repository 'PolarLipidZC'. When the download is finished, unzip the 'polarlipidzc-master' folder to a location on your computer. Check to make sure R (required) and Jupyter Notebooks with an R kernel (recommended) is installed on your machine. Next, check the list of prerequisites R libraries in the "Prerequisites" section and ensure they are installed.

### Running the code

The scripts in this repository can be run using the 'IPL Zc notebook.ipynb' notebook (recommended), in RStudio, or in the R console.

**Using Jupyter Notebooks**

Open Jupyter Notebooks by opening your computer's command line interface and entering:

```
jupyter notebook
```

A browser tab should open automatically to display the Jupyter interface. Use this interface to navigate to the 'polarlipidzc-master' folder and then open the 'IPL Zc notebook.ipynb' notebook. Select 'Restart & Run All' from the 'Kernel' dropdown menu to run the notebook. Wait for the notebook to finish.

**Using RStudio or R console**

Open RStudio or the R console. Change the working directory to the unzipped 'polarlipidzc-master' folder, then execute the following commands in order:

```
source("scripts//IPL_functions.r")
source("scripts//IPL_initiate_ZC_calcs.r")
source("scripts//IPL_initiate_monte.r")
source("scripts//IPL_observation_matrix.r")
source("scripts//pub_colors.r")
source("scripts//Fig4.r")
source("scripts//Fig5.r")
source("scripts//Fig6.r")
source("scripts//Fig7.r")
```

## Author

* **Grayson Boyer** - GitLab: [gmboyer](https://gitlab.com/gmboyer)