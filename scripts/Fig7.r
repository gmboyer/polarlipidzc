# load libraries
suppressMessages({
  library(msir)
})

# load current IPL_master
IPL_master <- readRDS("IPL_master.rds")


IPL_quant_sample_order <- c("Octopus OF2", "Octopus OF1", "Empress OF1",
                            "Empress OF2", "Empress OF3", "Empress OF4",
                            "Empress OF5", "Mound OF2", "Bison OF1",
                            "Bison OF2", "Bison OF3", "Bison OF4",
                            "Bison OF5", "Bison OF6", "Mound OF1",
                            "Mound OF3", "Mound OF4", "Mound OF5")

IPL_quant_order_temp <- c(59.8, 85.4, 82.1,
                          70.5, 60.7, 51.6,
                          38.1, 77.3, 89.0,
                          80.2, 73.7, 62.7,
                          37.2, 27.6, 91.0,
                          64.8, 53.0, 35.1)

O2 <- c(1.03E-4, 1.56E-5, 1.19E-5,
        2.47E-5, 3.75E-5, 4.06E-5,
        1.06E-4, 6.88E-5, 6.25E-6,
        2.19E-5, 3.44E-5, 7.19E-5,
        1.78E-4, 1.03E-4, 1.19E-5,
        4.38E-5, 1.13E-4, 2.16E-4)

logO2 <- log10(O2)

IPL_ZC_full <- IPL_master[[length(IPL_master)]][["IPL_ZCave"]]
IPL_ZC_head_only <- IPL_master[[length(IPL_master)]][["weighted_head_ZC"]]
IPL_ZC_backbone_only <- IPL_master[[length(IPL_master)]][["weighted_backbone_ZC"]]
IPL_ZC_chain_only <- IPL_master[[length(IPL_master)]][["weighted_chain_ZC"]]

IPL_df <- data.frame(TempC = IPL_quant_order_temp,
                     logO2 = logO2,
                     ZC = IPL_ZC_full,
                     IPL_ZC_head_only = IPL_ZC_head_only,
                     IPL_ZC_backbone_only = IPL_ZC_backbone_only,
                     IPL_ZC_chain_only = IPL_ZC_chain_only)


# pch_bison_filled <- 19 # Black circle
pch_bison_empty <- 21 # White circle
# pch_mound_filled <- 17 # Black triangle
pch_mound_empty <- 24 # White triangle
# pch_empress_filled <- 15 # Black square
pch_empress_empty <- 22 # White square
# pch_octopus_filled <- 18 # Black diamond
pch_octopus_empty <- 23 # White diamond

# get error bars from ZC distribution in IPL_master_monte
IPL_master_monte <- readRDS("IPL_master_monte_area_RF.rds")

monte_results <- data.frame()
for(i in 1:length(IPL_master_monte)){
  ZC_full <- data.frame(IPL_quant_sample_order, IPL_quant_order_temp, logO2, as.vector(IPL_master_monte[[i]][["weighted_IPL_ZC"]]), rep("ZC_full", 18))
  ZC_head <- data.frame(IPL_quant_sample_order, IPL_quant_order_temp, logO2, as.vector(IPL_master_monte[[i]][["weighted_head_ZC"]]), rep("ZC_head", 18))
  ZC_backbone <- data.frame(IPL_quant_sample_order, IPL_quant_order_temp, logO2, as.vector(IPL_master_monte[[i]][["weighted_backbone_ZC"]]), rep("ZC_backbone", 18))
  ZC_chain <- data.frame(IPL_quant_sample_order, IPL_quant_order_temp, logO2, as.vector(IPL_master_monte[[i]][["weighted_chain_ZC"]]), rep("ZC_chain", 18))
  colnames(ZC_full) <- c("sample", "temperature", "logO2", "ZC_result", "ZC_type")
  colnames(ZC_head) <- c("sample", "temperature", "logO2", "ZC_result", "ZC_type")
  colnames(ZC_backbone) <- c("sample", "temperature", "logO2", "ZC_result", "ZC_type")
  colnames(ZC_chain) <- c("sample", "temperature", "logO2", "ZC_result", "ZC_type")
  monte_results <- rbind(monte_results, ZC_full)
  monte_results <- rbind(monte_results, ZC_head)
  monte_results <- rbind(monte_results, ZC_backbone)
  monte_results <- rbind(monte_results, ZC_chain)
}

bison_monte_results <- monte_results[grep("Bison", monte_results$sample), ]
mound_monte_results <- monte_results[grep("Mound", monte_results$sample), ]
empress_monte_results <- monte_results[grep("Empress", monte_results$sample), ]
octopus_monte_results <- monte_results[grep("Octopus", monte_results$sample), ]

suppressWarnings({
  bison_monte_aves <- aggregate(bison_monte_results, by=list(bison_monte_results$sample,bison_monte_results$ZC_type), FUN=mean, na.rm=TRUE)
  mound_monte_aves <- aggregate(mound_monte_results, by=list(mound_monte_results$sample,mound_monte_results$ZC_type), FUN=mean, na.rm=TRUE)
  empress_monte_aves <- aggregate(empress_monte_results, by=list(empress_monte_results$sample,empress_monte_results$ZC_type), FUN=mean, na.rm=TRUE)
  octopus_monte_aves <- aggregate(octopus_monte_results, by=list(octopus_monte_results$sample,octopus_monte_results$ZC_type), FUN=mean, na.rm=TRUE)
}) # end of suppressMessages()

sample_lower_bar_full <- c()
sample_upper_bar_full <- c()
sample_lower_bar_head <- c()
sample_upper_bar_head <- c()
sample_lower_bar_backbone <- c()
sample_upper_bar_backbone <- c()
sample_lower_bar_chain <- c()
sample_upper_bar_chain <- c()
for(sample in unique(as.character(monte_results$sample))){
  sample_stdev_full <- sd(monte_results[monte_results["sample"] == sample & monte_results["ZC_type"] == "ZC_full", "ZC_result"])
  sample_lower_bar_full <- c(sample_lower_bar_full, sample_stdev_full)
  sample_upper_bar_full <- c(sample_upper_bar_full, sample_stdev_full)
  sample_stdev_head <- sd(monte_results[monte_results["sample"] == sample & monte_results["ZC_type"] == "ZC_head", "ZC_result"])
  sample_lower_bar_head <- c(sample_lower_bar_head, sample_stdev_head)
  sample_upper_bar_head <- c(sample_upper_bar_head, sample_stdev_head)
  sample_stdev_backbone <- sd(monte_results[monte_results["sample"] == sample & monte_results["ZC_type"] == "ZC_backbone", "ZC_result"])
  sample_lower_bar_backbone <- c(sample_lower_bar_backbone, sample_stdev_backbone)
  sample_upper_bar_backbone <- c(sample_upper_bar_backbone, sample_stdev_backbone)
  sample_stdev_chain <- sd(monte_results[monte_results["sample"] == sample & monte_results["ZC_type"] == "ZC_chain", "ZC_result"])
  sample_lower_bar_chain <- c(sample_lower_bar_chain, sample_stdev_chain)
  sample_upper_bar_chain <- c(sample_upper_bar_chain, sample_stdev_chain)

}
IPL_df["full lower bar"] <- sample_lower_bar_full
IPL_df["full upper bar"] <- sample_upper_bar_full
IPL_df["head lower bar"] <- sample_lower_bar_head
IPL_df["head upper bar"] <- sample_upper_bar_head
IPL_df["backbone lower bar"] <- sample_lower_bar_backbone
IPL_df["backbone upper bar"] <- sample_upper_bar_backbone
IPL_df["chain lower bar"] <- sample_lower_bar_chain
IPL_df["chain upper bar"] <- sample_upper_bar_chain



# colors (references 'scripts//pub_colors.r')
full_col <- cb_cols_drk[1]
head_col <- cb_cols_drk[4]
backbone_col <- cb_cols_drk[6]
chain_col <- cb_cols_drk[7]
full_col_shade <- cb_cols_medlit[1]
head_col_shade <- cb_cols_medlit[4]
backbone_col_shade <- cb_cols_medlit[6]
chain_col_shade <- cb_cols_medlit[7]


# Temperature as x axis, ZC as y axis
# dev.new(width = 10, height = 10)

# setEPS()
# postscript("Fig7.eps", width=6, height=5)

dev.new(width = 6, height = 5)
# jpeg('Fig7.jpg', width=6, height=5, units="in", res=300)
pdf(file="Fig7.pdf", width = 6, height = 5) # create pdf

# png(file="scatterplot - weighted ZC of IPL components vs temp and logO2.png", width=6, height=5, units = 'in', res = 600)
# par(mfrow = c(1, 2), mar = c(4.1, 4.1, 1.1, 1.1))

# One figure in row 1 and two figures in row 2
# row 1 is 1/3 the height of row 2
# column 2 is 1/4 the width of the column 1
layout(matrix(c(1,2,1,2), nrow = 1, ncol = 2, byrow = TRUE), widths=c(4.1 + 11 + 0, 1.1 + 11 + 1.1))
par(mar = c(4.1, 4.1, 1.1, 0.1))
par(xpd=FALSE) # prevent objects from appearing outside of plot region

plot(0, xlim = c(20, 100), ylim = c(-2, 0.5),
  # ylab = expression("Average Z"[C]),
  ylab = expression("Z"[C]),
  xlab = expression(paste("Temperature, ", degree, "C", sep = "")))




loess_pred <- function(x, y, color){
  model_head <- loess.sd(y ~ x, nsigma=1.96, span=.9) # 95% prediction interval
  polygon(c(rev(model_head$x), model_head$x), c(rev(model_head$upper), model_head$lower), col = color, border = NA, xpd = FALSE)
  lines(model_head$x, model_head$y)
}

lm_pred <- function(x, y, color){
  lm_model <- lm(y ~ x)
  newx <- seq(min(x), max(x), length.out=100)
  preds <- predict(lm_model, newdata = data.frame(x=newx),
                   interval = 'prediction')
  polygon(c(rev(newx), newx), c(rev(preds[ ,3]), preds[ ,2]), col = color, border = NA, xpd = FALSE)
  abline(lm_model)
}

y <- monte_results[monte_results["ZC_type"] == "ZC_full", "ZC_result"]
x <- monte_results[monte_results["ZC_type"] == "ZC_full", "temperature"]
lm_pred(x, y, full_col_shade)

y <- monte_results[monte_results["ZC_type"] == "ZC_head", "ZC_result"]
x <- monte_results[monte_results["ZC_type"] == "ZC_head", "temperature"]
loess_pred(x, y, head_col_shade)
# lm_pred(x, y, head_col_shade)

y <- monte_results[monte_results["ZC_type"] == "ZC_backbone", "ZC_result"]
x <- monte_results[monte_results["ZC_type"] == "ZC_backbone", "temperature"]
lm_pred(x, y, backbone_col_shade)

y <- monte_results[monte_results["ZC_type"] == "ZC_chain", "ZC_result"]
x <- monte_results[monte_results["ZC_type"] == "ZC_chain", "temperature"]
lm_pred(x, y, chain_col_shade)

# # Add red dashed line at ZC 0.1
# segments(x0 = 10, x1= 110, y0=0.1, y1=0.1, col = "red", lwd = 2, lty = "dashed")

if (length(grep("Bison", rownames(IPL_df)) > 0)){
  these_rows <- grep("Bison", rownames(IPL_df))
  arrows(bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_full", "temperature"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_full", "ZC_result"] - IPL_df[these_rows, "full lower bar"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_full", "temperature"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_full", "ZC_result"] + IPL_df[these_rows, "full upper bar"], length=0.02, angle=90, code=3)
  arrows(bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_head", "temperature"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_head", "ZC_result"] - IPL_df[these_rows, "head lower bar"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_head", "temperature"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_head", "ZC_result"] + IPL_df[these_rows, "head upper bar"], length=0.02, angle=90, code=3)
  arrows(bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_backbone", "temperature"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_backbone", "ZC_result"] - IPL_df[these_rows, "backbone lower bar"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_backbone", "temperature"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_backbone", "ZC_result"] + IPL_df[these_rows, "backbone upper bar"], length=0.02, angle=90, code=3)
  arrows(bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_chain", "temperature"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_chain", "ZC_result"] - IPL_df[these_rows, "chain lower bar"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_chain", "temperature"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_chain", "ZC_result"] + IPL_df[these_rows, "chain upper bar"], length=0.02, angle=90, code=3)
}
if (length(grep("Mound", rownames(IPL_df)) > 0)){
  these_rows <- grep("Mound", rownames(IPL_df))
  arrows(mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_full", "temperature"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_full", "ZC_result"] - IPL_df[these_rows, "full lower bar"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_full", "temperature"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_full", "ZC_result"] + IPL_df[these_rows, "full upper bar"], length=0.02, angle=90, code=3)
  arrows(mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_head", "temperature"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_head", "ZC_result"] - IPL_df[these_rows, "head lower bar"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_head", "temperature"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_head", "ZC_result"] + IPL_df[these_rows, "head upper bar"], length=0.02, angle=90, code=3)
  arrows(mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_backbone", "temperature"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_backbone", "ZC_result"] - IPL_df[these_rows, "backbone lower bar"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_backbone", "temperature"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_backbone", "ZC_result"] + IPL_df[these_rows, "backbone upper bar"], length=0.02, angle=90, code=3)
  arrows(mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_chain", "temperature"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_chain", "ZC_result"] - IPL_df[these_rows, "chain lower bar"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_chain", "temperature"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_chain", "ZC_result"] + IPL_df[these_rows, "chain upper bar"], length=0.02, angle=90, code=3)
}
if (length(grep("Empress", rownames(IPL_df)) > 0)){
  these_rows <- grep("Empress", rownames(IPL_df))
  arrows(empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_full", "temperature"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_full", "ZC_result"] - IPL_df[these_rows, "full lower bar"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_full", "temperature"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_full", "ZC_result"] + IPL_df[these_rows, "full upper bar"], length=0.02, angle=90, code=3)
  arrows(empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_head", "temperature"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_head", "ZC_result"] - IPL_df[these_rows, "head lower bar"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_head", "temperature"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_head", "ZC_result"] + IPL_df[these_rows, "head upper bar"], length=0.02, angle=90, code=3)
  arrows(empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_backbone", "temperature"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_backbone", "ZC_result"] - IPL_df[these_rows, "backbone lower bar"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_backbone", "temperature"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_backbone", "ZC_result"] + IPL_df[these_rows, "backbone upper bar"], length=0.02, angle=90, code=3)
  arrows(empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_chain", "temperature"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_chain", "ZC_result"] - IPL_df[these_rows, "chain lower bar"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_chain", "temperature"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_chain", "ZC_result"] + IPL_df[these_rows, "chain upper bar"], length=0.02, angle=90, code=3)
  }
if (length(grep("Octopus", rownames(IPL_df)) > 0)){
  these_rows <- grep("Octopus", rownames(IPL_df))
  arrows(octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_full", "temperature"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_full", "ZC_result"] - IPL_df[these_rows, "full lower bar"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_full", "temperature"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_full", "ZC_result"] + IPL_df[these_rows, "full upper bar"], length=0.02, angle=90, code=3)
  arrows(octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_head", "temperature"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_head", "ZC_result"] - IPL_df[these_rows, "head lower bar"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_head", "temperature"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_head", "ZC_result"] + IPL_df[these_rows, "head upper bar"], length=0.02, angle=90, code=3)
  arrows(octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_backbone", "temperature"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_backbone", "ZC_result"] - IPL_df[these_rows, "backbone lower bar"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_backbone", "temperature"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_backbone", "ZC_result"] + IPL_df[these_rows, "backbone upper bar"], length=0.02, angle=90, code=3)
  arrows(octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_chain", "temperature"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_chain", "ZC_result"] - IPL_df[these_rows, "chain lower bar"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_chain", "temperature"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_chain", "ZC_result"] + IPL_df[these_rows, "chain upper bar"], length=0.02, angle=90, code=3)
  }
if (length(grep("Bison", rownames(IPL_df)) > 0)){
  these_rows <- grep("Bison", rownames(IPL_df))
  points(IPL_df[these_rows, "TempC"], IPL_df[these_rows, "ZC"], pch = pch_bison_empty, bg = full_col, col = "black")
  points(IPL_df[these_rows, "TempC"], IPL_df[these_rows, "IPL_ZC_head_only"], pch = pch_bison_empty, bg = head_col, col = "black")
  points(IPL_df[these_rows, "TempC"], IPL_df[these_rows, "IPL_ZC_backbone_only"], pch = pch_bison_empty, bg = backbone_col, col = "black")
  points(IPL_df[these_rows, "TempC"], IPL_df[these_rows, "IPL_ZC_chain_only"], pch = pch_bison_empty, bg = chain_col, col = "black")
}
if (length(grep("Mound", rownames(IPL_df)) > 0)){
  these_rows <- grep("Mound", rownames(IPL_df))
  points(IPL_df[these_rows, "TempC"], IPL_df[these_rows, "ZC"], pch = pch_mound_empty, bg = full_col, col = "black")
  points(IPL_df[these_rows, "TempC"], IPL_df[these_rows, "IPL_ZC_head_only"], pch = pch_mound_empty, bg = head_col, col = "black")
  points(IPL_df[these_rows, "TempC"], IPL_df[these_rows, "IPL_ZC_backbone_only"], pch = pch_mound_empty, bg = backbone_col, col = "black")
  points(IPL_df[these_rows, "TempC"], IPL_df[these_rows, "IPL_ZC_chain_only"], pch = pch_mound_empty, bg = chain_col, col = "black")
}
if (length(grep("Empress", rownames(IPL_df)) > 0)){
  these_rows <- grep("Empress", rownames(IPL_df))
  points(IPL_df[these_rows, "TempC"], IPL_df[these_rows, "ZC"], pch = pch_empress_empty, bg = full_col, col = "black")
  points(IPL_df[these_rows, "TempC"], IPL_df[these_rows, "IPL_ZC_head_only"], pch = pch_empress_empty, bg = head_col, col = "black")
  points(IPL_df[these_rows, "TempC"], IPL_df[these_rows, "IPL_ZC_backbone_only"], pch = pch_empress_empty, bg = backbone_col, col = "black")
  points(IPL_df[these_rows, "TempC"], IPL_df[these_rows, "IPL_ZC_chain_only"], pch = pch_empress_empty, bg = chain_col, col = "black")
}
if (length(grep("Octopus", rownames(IPL_df)) > 0)){
  these_rows <- grep("Octopus", rownames(IPL_df))
  points(IPL_df[these_rows, "TempC"], IPL_df[these_rows, "ZC"], pch = pch_octopus_empty, bg = full_col, col = "black")
  points(IPL_df[these_rows, "TempC"], IPL_df[these_rows, "IPL_ZC_head_only"], pch = pch_octopus_empty, bg = head_col, col = "black")
  points(IPL_df[these_rows, "TempC"], IPL_df[these_rows, "IPL_ZC_backbone_only"], pch = pch_octopus_empty, bg = backbone_col, col = "black")
  points(IPL_df[these_rows, "TempC"], IPL_df[these_rows, "IPL_ZC_chain_only"], pch = pch_octopus_empty, bg = chain_col, col = "black")
}
jeff_2011_temps <- c(93.3, 79.4, 67.5, 65.3, 57.1)
jeff_2011_O2ppm <- c(0.173, 0.776, 0.9, 1.6, 2.8)
jeff_2011_logO2 <- log10((jeff_2011_O2ppm/1000)/15.999)
jeff_2011_ZC <- c(-0.208, -0.196, -0.171, -0.154, -0.156)
prot_col <- "yellow"

text(88, 0.46, "Backbones", adj = 1, cex=0.75, font=3)
text(88, -.5, "Headgroups", adj = 1, cex=0.75, font=3) # srt=-8 for rotation
text(88, -1.39, "IPLs", adj = 1, srt=-10, cex=0.75, font=3)
text(88, -1.81, "Alkyl chains", adj = 1, srt=-9, cex=0.75, font=3)

par(mar = c(4.1, 1.1, 1.1, 1.1))
plot(0, xlim = c(-5.5, -3.5), ylim = c(-2, 0.5),
  # ylab = expression("Average Z"[C]),
  yaxt='n',
  ylab = "",
  xlab = expression("log[O"[2]*", molal]"))
  # xlab = expression(paste("log ", O[2], ", log mol"%.%"kg"^{-1})))

y <- monte_results[monte_results["ZC_type"] == "ZC_full", "ZC_result"]
x <- monte_results[monte_results["ZC_type"] == "ZC_full", "logO2"]
lm_pred(x, y, full_col_shade)

y <- monte_results[monte_results["ZC_type"] == "ZC_head", "ZC_result"]
x <- monte_results[monte_results["ZC_type"] == "ZC_head", "logO2"]
loess_pred(x, y, head_col_shade)

y <- monte_results[monte_results["ZC_type"] == "ZC_backbone", "ZC_result"]
x <- monte_results[monte_results["ZC_type"] == "ZC_backbone", "logO2"]
lm_pred(x, y, backbone_col_shade)

y <- monte_results[monte_results["ZC_type"] == "ZC_chain", "ZC_result"]
x <- monte_results[monte_results["ZC_type"] == "ZC_chain", "logO2"]
lm_pred(x, y, chain_col_shade)

axis(side =2, labels = F)


if (length(grep("Bison", rownames(IPL_df)) > 0)){
  these_rows <- grep("Bison", rownames(IPL_df))
  arrows(bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_full", "logO2"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_full", "ZC_result"] - IPL_df[these_rows, "full lower bar"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_full", "logO2"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_full", "ZC_result"] + IPL_df[these_rows, "full upper bar"], length=0.02, angle=90, code=3)
  arrows(bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_head", "logO2"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_head", "ZC_result"] - IPL_df[these_rows, "head lower bar"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_head", "logO2"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_head", "ZC_result"] + IPL_df[these_rows, "head upper bar"], length=0.02, angle=90, code=3)
  arrows(bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_backbone", "logO2"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_backbone", "ZC_result"] - IPL_df[these_rows, "backbone lower bar"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_backbone", "logO2"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_backbone", "ZC_result"] + IPL_df[these_rows, "backbone upper bar"], length=0.02, angle=90, code=3)
  arrows(bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_chain", "logO2"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_chain", "ZC_result"] - IPL_df[these_rows, "chain lower bar"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_chain", "logO2"], bison_monte_aves[bison_monte_aves[, "Group.2"]=="ZC_chain", "ZC_result"] + IPL_df[these_rows, "chain upper bar"], length=0.02, angle=90, code=3)
}
if (length(grep("Mound", rownames(IPL_df)) > 0)){
  these_rows <- grep("Mound", rownames(IPL_df))
  arrows(mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_full", "logO2"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_full", "ZC_result"] - IPL_df[these_rows, "full lower bar"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_full", "logO2"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_full", "ZC_result"] + IPL_df[these_rows, "full upper bar"], length=0.02, angle=90, code=3)
  arrows(mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_head", "logO2"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_head", "ZC_result"] - IPL_df[these_rows, "head lower bar"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_head", "logO2"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_head", "ZC_result"] + IPL_df[these_rows, "head upper bar"], length=0.02, angle=90, code=3)
  arrows(mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_backbone", "logO2"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_backbone", "ZC_result"] - IPL_df[these_rows, "backbone lower bar"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_backbone", "logO2"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_backbone", "ZC_result"] + IPL_df[these_rows, "backbone upper bar"], length=0.02, angle=90, code=3)
  arrows(mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_chain", "logO2"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_chain", "ZC_result"] - IPL_df[these_rows, "chain lower bar"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_chain", "logO2"], mound_monte_aves[mound_monte_aves[, "Group.2"]=="ZC_chain", "ZC_result"] + IPL_df[these_rows, "chain upper bar"], length=0.02, angle=90, code=3)
}
if (length(grep("Empress", rownames(IPL_df)) > 0)){
  these_rows <- grep("Empress", rownames(IPL_df))
  arrows(empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_full", "logO2"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_full", "ZC_result"] - IPL_df[these_rows, "full lower bar"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_full", "logO2"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_full", "ZC_result"] + IPL_df[these_rows, "full upper bar"], length=0.02, angle=90, code=3)
  arrows(empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_head", "logO2"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_head", "ZC_result"] - IPL_df[these_rows, "head lower bar"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_head", "logO2"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_head", "ZC_result"] + IPL_df[these_rows, "head upper bar"], length=0.02, angle=90, code=3)
  arrows(empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_backbone", "logO2"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_backbone", "ZC_result"] - IPL_df[these_rows, "backbone lower bar"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_backbone", "logO2"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_backbone", "ZC_result"] + IPL_df[these_rows, "backbone upper bar"], length=0.02, angle=90, code=3)
  arrows(empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_chain", "logO2"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_chain", "ZC_result"] - IPL_df[these_rows, "chain lower bar"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_chain", "logO2"], empress_monte_aves[empress_monte_aves[, "Group.2"]=="ZC_chain", "ZC_result"] + IPL_df[these_rows, "chain upper bar"], length=0.02, angle=90, code=3)
  }
if (length(grep("Octopus", rownames(IPL_df)) > 0)){
  these_rows <- grep("Octopus", rownames(IPL_df))
  arrows(octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_full", "logO2"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_full", "ZC_result"] - IPL_df[these_rows, "full lower bar"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_full", "logO2"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_full", "ZC_result"] + IPL_df[these_rows, "full upper bar"], length=0.02, angle=90, code=3)
  arrows(octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_head", "logO2"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_head", "ZC_result"] - IPL_df[these_rows, "head lower bar"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_head", "logO2"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_head", "ZC_result"] + IPL_df[these_rows, "head upper bar"], length=0.02, angle=90, code=3)
  arrows(octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_backbone", "logO2"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_backbone", "ZC_result"] - IPL_df[these_rows, "backbone lower bar"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_backbone", "logO2"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_backbone", "ZC_result"] + IPL_df[these_rows, "backbone upper bar"], length=0.02, angle=90, code=3)
  arrows(octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_chain", "logO2"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_chain", "ZC_result"] - IPL_df[these_rows, "chain lower bar"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_chain", "logO2"], octopus_monte_aves[octopus_monte_aves[, "Group.2"]=="ZC_chain", "ZC_result"] + IPL_df[these_rows, "chain upper bar"], length=0.02, angle=90, code=3)
  }
if (length(grep("Bison", rownames(IPL_df)) > 0)){
  these_rows <- grep("Bison", rownames(IPL_df))
  points(IPL_df[these_rows, "logO2"], IPL_df[these_rows, "ZC"], pch = pch_bison_empty, bg = full_col, col = "black")
  points(IPL_df[these_rows, "logO2"], IPL_df[these_rows, "IPL_ZC_head_only"], pch = pch_bison_empty, bg = head_col, col = "black")
  points(IPL_df[these_rows, "logO2"], IPL_df[these_rows, "IPL_ZC_backbone_only"], pch = pch_bison_empty, bg = backbone_col, col = "black")
  points(IPL_df[these_rows, "logO2"], IPL_df[these_rows, "IPL_ZC_chain_only"], pch = pch_bison_empty, bg = chain_col, col = "black")
}
if (length(grep("Mound", rownames(IPL_df)) > 0)){
  these_rows <- grep("Mound", rownames(IPL_df))
  points(IPL_df[these_rows, "logO2"], IPL_df[these_rows, "ZC"], pch = pch_mound_empty, bg = full_col, col = "black")
  points(IPL_df[these_rows, "logO2"], IPL_df[these_rows, "IPL_ZC_head_only"], pch = pch_mound_empty, bg = head_col, col = "black")
  points(IPL_df[these_rows, "logO2"], IPL_df[these_rows, "IPL_ZC_backbone_only"], pch = pch_mound_empty, bg = backbone_col, col = "black")
  points(IPL_df[these_rows, "logO2"], IPL_df[these_rows, "IPL_ZC_chain_only"], pch = pch_mound_empty, bg = chain_col, col = "black")
}
if (length(grep("Empress", rownames(IPL_df)) > 0)){
  these_rows <- grep("Empress", rownames(IPL_df))
  points(IPL_df[these_rows, "logO2"], IPL_df[these_rows, "ZC"], pch = pch_empress_empty, bg = full_col, col = "black")
  points(IPL_df[these_rows, "logO2"], IPL_df[these_rows, "IPL_ZC_head_only"], pch = pch_empress_empty, bg = head_col, col = "black")
  points(IPL_df[these_rows, "logO2"], IPL_df[these_rows, "IPL_ZC_backbone_only"], pch = pch_empress_empty, bg = backbone_col, col = "black")
  points(IPL_df[these_rows, "logO2"], IPL_df[these_rows, "IPL_ZC_chain_only"], pch = pch_empress_empty, bg = chain_col, col = "black")
}
if (length(grep("Octopus", rownames(IPL_df)) > 0)){
  these_rows <- grep("Octopus", rownames(IPL_df))
  points(IPL_df[these_rows, "logO2"], IPL_df[these_rows, "ZC"], pch = pch_octopus_empty, bg = full_col, col = "black")
  points(IPL_df[these_rows, "logO2"], IPL_df[these_rows, "IPL_ZC_head_only"], pch = pch_octopus_empty, bg = head_col, col = "black")
  points(IPL_df[these_rows, "logO2"], IPL_df[these_rows, "IPL_ZC_backbone_only"], pch = pch_octopus_empty, bg = backbone_col, col = "black")
  points(IPL_df[these_rows, "logO2"], IPL_df[these_rows, "IPL_ZC_chain_only"], pch = pch_octopus_empty, bg = chain_col, col = "black")
}

text(-5.1, 0.46, "Backbones", adj = 0, cex=0.75, font=3)
text(-5.1, -.5, "Headgroups", adj = 0, srt=0, cex=0.75, font=3)
text(-5.1, -1.39, "IPLs", adj = 0, srt=10, cex=0.75, font=3)
text(-5.1, -1.81, "Alkyl chains", adj = 0, srt=9, cex=0.75, font=3)

dev.off()
print("Created Fig7.pdf")