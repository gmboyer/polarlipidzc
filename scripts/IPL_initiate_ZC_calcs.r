# IPL processing parameters
quant_sample_order <- c("Octopus OF2", "Octopus OF1", "Empress OF1",
  "Empress OF2", "Empress OF3", "Empress OF4", "Empress OF5",
  "Mound OF2", "Bison OF1", "Bison OF2", "Bison OF3", "Bison OF4",
  "Bison OF5", "Bison OF6", "Mound OF1", "Mound OF3", "Mound OF4",
  "Mound OF5")

GDGTs <- c("GDGTs subcat: 1G-GDGT", "GDGTs subcat: 1G-P-GDGT",
           "GDGTs subcat: 2G-GDGT", "GDGTs subcat: 2G-P-GDGT",
           "GDGTs subcat: 3G-GDGT", "GDGTs subcat: 3G-P-GDGT",
           "GDGTs subcat: 4G-GDGT")

IPLs_noGDGTs <- c(
  "AR-IPLs subcat: 2G-P-AR", "AR-IPLs subcat: G-MeNG-G-P-AR",
  "AR-IPLs subcat: G-NG-G-P-AR", "AR-IPLs subcat: MeNG-G-P-AR",
  "AR-IPLs subcat: NG-G-P-AR", "AR-IPLs subcat: PE-AR",
  "GA-DAG", "PG-DAG", "PI-DAG", "PI-AEG", "PI-DEG",
  "BL-DAG", "1G-Cer", "APT-DAG", "APT-AEG", "APT-DEG",
  "OL", "SQ-DAG", "1G-DAG", "1G-AEG", "1G-DEG",
  "2G-DAG", "2G-AEG", "2G-DEG", "3G-DAG", "3G-DEG",
  "PE-DAG", "PC-DAG", "G-NG-DAG", "G-NG-DEG",
  "3GNAcG-G-DEG", "NAcG-P-DAG", "NAcG-P-DEG",
  "OL-1OH", "TM-OL", "TM-OL-1OH", "PE-Cer", "PME-DAG", "PDME-DAG",
  "NAcG-G-DAG", "NAcG-G-DEG", "TM-Lysine 1", "TM-Lysine 2",
  "DPG", "PS-DAG", "2G-P-DEG", "PI-Cer",
  "NG-GA-DAG", "NG-GA-AEG", "NG-GA-DEG", "223-DAG", "G-GA-DAG")

all_IPLs <- c(GDGTs, IPLs_noGDGTs)

subgroup_colname <- "Headgroup"
workbook_name <- "IPL_data.xlsx"
IPLworkbook_directory <- "data//"

# Create save the 'IPL_master' object
print("Creating IPL_master.rds")

IPL_grouping <- list(c(all_IPLs))

IPL_master <- IPL_process(IPL_grouping, workbook_name, quant_sample_order,
                          IPLworkbook_directory, subgroup_colname)


saveRDS(IPL_master, "IPL_master.rds")

print("IPL_master.rds created.")

# Create save the 'IPL_master_noGDGT' object (required for Fig 7)
print("Creating IPL_master_no_GDGTs.rds")

IPL_grouping <- list(c(IPLs_noGDGTs))

IPL_master_noGDGT <- IPL_process(IPL_grouping, workbook_name, quant_sample_order,
                                 IPLworkbook_directory, subgroup_colname)

saveRDS(IPL_master_noGDGT, "IPL_master_no_GDGTs.rds")

print("IPL_master_no_GDGTs.rds created.")

# Create save the 'IPL_master_GDGT' object (not used)
print("Creating IPL_master_GDGTs.rds")

IPL_grouping <- list(c(GDGTs))

IPL_master_noGDGT <- IPL_process(IPL_grouping, workbook_name, quant_sample_order,
                                 IPLworkbook_directory, subgroup_colname)

saveRDS(IPL_master_noGDGT, "IPL_master_GDGTs.rds")

print("IPL_master_GDGTs.rds created.")