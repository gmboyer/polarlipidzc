
### NOTE: A previous version of ggplot2 is needed to prevent the "Octopus" label from getting cut off.
### See: https://github.com/tidyverse/ggplot2/issues/2772
### Turning off 'clip' for i in gp$grob[[i]]$layout$clip does not fix the problem in later versions of ggplot
### as one might expect so the easiest solution is to install ggplot2 version 3.1.0:
# url <- "https://cran.r-project.org/src/contrib/Archive/ggplot2/ggplot2_3.1.0.tar.gz"
# install.packages(url, repos=NULL, type="source")


### Plot of relative headgroup abundances

cutoff <- 2 # IPLs with % abundance less than this are lumped together (default 2%)

# load libraries
suppressMessages({
  library(reshape)
  library(ggplot2)
})

# Load table of peak areas
df <- read.csv("IPL_abund.csv") # for RF-corrected peak areas
df <- df[, 2:ncol(df)]

# sum by headgroup and backbone
df_agg <- aggregate(cbind(df[, 6:ncol(df)]), by=list(IPL=df$names), FUN=sum)
rownames(df_agg) <- as.character(df_agg[, "IPL"])
df_agg <- df_agg[2:ncol(df_agg)]

# rename 'DPG' as 'DPG-DAG'
rownames(df_agg)[which(rownames(df_agg) == 'DPG')] <- "DPG-DAG"

# handle site names
names(df_agg) <- sub("Bison.OF", "BP", names(df_agg))
names(df_agg) <- sub("Mound.OF", "MS", names(df_agg))
names(df_agg) <- sub("Empress.OF", "EP", names(df_agg))
names(df_agg) <- sub("Octopus.OF", "OS", names(df_agg))
site_names <- names(df_agg)

# sum GDGT ring rows (1G-GDGT-0 + 1G-GDGT-1 + ...)
GDGTs <- c("1G-GDGT", "2G-GDGT", "3G-GDGT", "2G-P-GDGT")
for(GDGT in GDGTs){
  df_agg_noGDGT <- df_agg[-which(grepl(GDGT, rownames(df_agg))), ]
  df_agg_GDGTsum <- df_agg[which(grepl(GDGT, rownames(df_agg))), ]
  df_agg_GDGTsum <- colSums(df_agg_GDGTsum)
  df_agg <- rbind(df_agg_noGDGT, df_agg_GDGTsum)
  rownames(df_agg)[nrow(df_agg)] <- GDGT
}

# convert to percents
df_agg_perc <- as.data.frame(prop.table(as.matrix(df_agg), margin=2)*100)

df_agg_perc_cutoff <- df_agg_perc
df_agg_perc_cutoff[df_agg_perc > cutoff] <- 0

# lump < 5% into "other"
df_agg_perc[df_agg_perc < cutoff] <- 0
df_agg_perc <- rbind(df_agg_perc, colSums(df_agg_perc_cutoff))
rownames(df_agg_perc)[nrow(df_agg_perc)] <- paste0("< ", cutoff, "%")

# remove rows containing only 0's
nrow(df_agg_perc)
df_agg_perc <- df_agg_perc[!!rowSums(abs(df_agg_perc)),]
nrows <- nrow(df_agg_perc)
nrows

# remove "[H+]" from rownames
rownames(df_agg_perc) <- sub(" \\[H\\+\\]", "", rownames(df_agg_perc))
df <- df_agg_perc

# order IPLs by glyco, glycophospho, phospho, aminolipid, unknown
glyco <- c()
glycophospho <- c()
phospho <- c()
amino <- c()
for(row in rownames(df)){
  if(grepl("G\\-|GA|NG|SQ|PI", row)){
    if(grepl("G\\-P\\-", row)){
      rownames(df)[which(rownames(df)==row)] <- paste0(2, "--", row)
      glycophospho <- c(glycophospho, paste0(2, "--", row))
    } else if(grepl("PG", paste0(2, "--", row))){
      rownames(df)[which(rownames(df)==row)] <- paste0(3, "--", row)
      phospho <- c(phospho, paste0(3, "--", row))
    } else if(grepl("PI", row)){
      rownames(df)[which(rownames(df)==row)] <- paste0(2, "--", row)
      glycophospho <- c(glycophospho, paste0(2, "--", row))
    } else {
      rownames(df)[which(rownames(df)==row)] <- paste0(1, "--", row)
      glyco <- c(glyco, paste0(1, "--", row))
    }
  } else if (grepl("APT|DPG|PC|PDME|PE|PG|PME|PS", row)){
    rownames(df)[which(rownames(df)==row)] <- paste0(3, "--", row)
    phospho <- c(phospho, paste0(3, "--", row))

  } else {
    rownames(df)[which(rownames(df)==row)] <- paste0(4, "--", row)
    amino <- c(amino, paste0(4, "--", row))
  }
}

# order IPLs by Unspecific Archaea, Aquificales, Phototrophs, Unspecific Bacteria and Eukaryotes
archaea <- c()
aquificales <- c()
phototrophs <- c()
unspecific <- c()
unknown <- c()

for(row in rownames(df)){
  if(grepl("GDGT", row)){
    rownames(df)[which(rownames(df)==row)] <- paste0(1, "--", row)
    archaea <- c(archaea, paste0(1, "--", row))
  } else if (grepl("AR", row)){
    rownames(df)[which(rownames(df)==row)] <- paste0(1, "--", row)
    archaea <- c(archaea, paste0(1, "--", row))
  } else if (grepl("PI\\-DEG|PI\\-AEG|APT\\-DEG|APT\\-AEG", row)){
    rownames(df)[which(rownames(df)==row)] <- paste0(2, "--", row)
    aquificales <- c(aquificales, paste0(2, "--", row))
  } else if (grepl("SQ\\-DAG", row)){
    rownames(df)[which(rownames(df)==row)] <- paste0(3, "--", row)
    phototrophs <- c(phototrophs, paste0(3, "--", row))
  } else if (grepl("223", row)){
    rownames(df)[which(rownames(df)==row)] <- paste0(5, "--", row)
    unknown <- c(unknown, paste0(5, "--", row))
  } else if (grepl("\\%", row)){
    rownames(df)[which(rownames(df)==row)] <- paste0(6, "--", row)
  # } else if (grepl("223", row)){
  #   rownames(df)[which(rownames(df)==row)] <- paste0(5, "--", row) 
  } else {
   rownames(df)[which(rownames(df)==row)] <- paste0(4, "--", row)
   unspecific <- c(unspecific, paste0(4, "--", row))
 }
}

# sort dataframe by rowname alphabetically
df <- df[order(rownames(df)),]

# assign colors, black by default
IPL_colors <- rep("#463F3F", nrows)
names(IPL_colors) <- rownames(df)
archaea_col <- "#386986"
aquificales_col <- "#B32F22"
phototrophs_col <- "#6D8B33"
unspecific_col <- "#DFB231"
unknown_col <- "#AA677A" # not used

# color archaea
cols <- intersect(names(IPL_colors), archaea)
col_vals <- colorRampPalette(c(archaea_col, archaea_col))(length(cols))
#col_vals <- rainbow(length(cols))
names(col_vals) <- cols
IPL_colors[names(col_vals)] <- col_vals

# color aquificales
cols <- intersect(names(IPL_colors), aquificales)
col_vals <- colorRampPalette(c(aquificales_col, aquificales_col))(length(cols))
#col_vals <- terrain.colors(length(cols))
names(col_vals) <- cols
IPL_colors[names(col_vals)] <- col_vals

# color phototrophs
cols <- intersect(names(IPL_colors), phototrophs)
col_vals <- colorRampPalette(c(phototrophs_col, phototrophs_col))(length(cols))
#col_vals <- topo.colors(length(cols))
names(col_vals) <- cols
IPL_colors[names(col_vals)] <- col_vals

# color unspecific
cols <- intersect(names(IPL_colors), unspecific)
col_vals <- colorRampPalette(c(unspecific_col, unspecific_col))(length(cols))
#col_vals <- topo.colors(length(cols))
names(col_vals) <- cols
IPL_colors[names(col_vals)] <- col_vals

# color unknown
cols <- intersect(names(IPL_colors), unknown)
col_vals <- colorRampPalette(c(unknown_col, unknown_col))(length(cols))
#col_vals <- topo.colors(length(cols))
names(col_vals) <- cols
IPL_colors[names(col_vals)] <- col_vals


# melt dataframe
df[, "IPL"] <- rownames(df)
df[, "IPL_id"] <- c(1:(length(df[, "IPL"])-1), "")
df_melt <- melt(df, id=c("IPL", "IPL_id"))

#swap variable and IPL
names(df_melt) <- c("IPL", "IPL_id", "site", "percent")
# df_melt <- df_melt[, c("site", "IPL", "percent")]

# delete rows with 0 for percent
nrow(df_melt)
df_melt <- df_melt[which(df_melt$percent != 0), ]
nrow(df_melt)

df_melt[, "spring"] <- "spring"
df_melt[which(grepl("BP", df_melt[, "site"])), "spring"] <- "Bison"
df_melt[which(grepl("MS", df_melt[, "site"])), "spring"] <- "Mound"
df_melt[which(grepl("EP", df_melt[, "site"])), "spring"] <- "Empress"
df_melt[which(grepl("OS", df_melt[, "site"])), "spring"] <- "Octopus"

df_melt_test <- df_melt

# # remove ordering from df_melt and color lists
df_melt[, "IPL"] <- gsub(".\\-\\-.\\-\\-", "", df_melt[, "IPL"])
df_melt[, "IPL"] <- gsub(".\\-\\-", "", df_melt[, "IPL"])
names(IPL_colors) <- gsub(".\\-\\-.\\-\\-", "", names(IPL_colors))
names(IPL_colors) <- gsub(".\\-\\-", "", names(IPL_colors))

IPL_colors[paste0("< ", cutoff, "%")] <- "#463F3F"

df_melt$IPL <- factor(df_melt$IPL, levels = names(IPL_colors))
df_melt$site <- factor(df_melt$site, levels = rev(site_names))

# create plot object
p <- ggplot(data=df_melt, aes(x=site, y=percent, fill=IPL, label = IPL_id)) +
  geom_bar(stat="identity", position = position_stack(reverse = TRUE), color="black", width = .85, size=.2) +
  scale_fill_manual(values=IPL_colors, labels=names(IPL_colors), breaks=names(IPL_colors), guide = guide_legend(title = NULL)) +
  scale_y_continuous(breaks=seq(0,100,25)) +
  labs(title = NULL, x = NULL, y = "Percent") +
  theme_minimal(base_size = 15) +
  theme(panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    legend.position="bottom",
    axis.text.x = element_text(color = "black"),
    axis.text.y = element_text(color = "black", margin=margin(r = -25)),
    axis.ticks.x = element_line(color = "black"),
    plot.title = element_text(color = "black", face = "bold", hjust = 0.5),
    strip.placement = "outside", # position spring names outside of site names
    strip.text.y = element_text(size = 15, colour = "black", margin=margin(r=5))
  ) +
  facet_grid(spring ~ ., scales = "free_y", space = "free_y", switch = "y") +
  geom_text(aes(label=IPL_id), size=3, na.rm=TRUE, show.legend = TRUE, position = position_stack(vjust = 0.5, reverse=TRUE)) +
  coord_flip()

gp <- ggplotGrob(p)

# assign labels to legend fill box
this_grob <- 0
for(i in 1:length(df[, "IPL"])){
  if(i == length(df[, "IPL"])){
    this_label <- "" # prevents <X% from being numbered
  } else {
    this_label <- as.character(i)
  }
  gp$grobs[24][[1]]$grobs[1][[1]]$grobs[5+this_grob][[1]]$label <- this_label
  this_grob <- this_grob + 3
}

# export plot
dev.new(width=10, height=7.6)
grid::grid.draw(gp)

pdf(file="Fig4.pdf", width=10, height=7.6)
grid::grid.draw(gp)
dev.off()

# jpeg('Fig4.jpg', width=10, height=7.6, units="in", res=300)
# grid::grid.draw(gp)
# dev.off()

print("Created Fig4.pdf")