# colors used in figures
cb_cols_drk <- c(rgb(0,   0,   0  , maxColorValue = 255), # 1. black
                 rgb(230, 159, 0  , maxColorValue = 255), # 2. orange
                 rgb(86,  180, 233, maxColorValue = 255), # 3. sky blue
                 rgb(0,   158, 115, maxColorValue = 255), # 4. bluish green
                 rgb(240, 228, 66 , maxColorValue = 255), # 5. yellow
                 rgb(0,   114, 178, maxColorValue = 255), # 6. blue
                 rgb(213, 94,  0  , maxColorValue = 255), # 7. vermillion
                 rgb(204, 121, 167, maxColorValue = 255)) # 8. reddish purple

cb_cols_med <- c(rgb(191, 191, 191, maxColorValue = 255), # 1. black
                 rgb(240, 205, 135, maxColorValue = 255), # 2. orange
                 rgb(164, 215, 248, maxColorValue = 255), # 3. sky blue
                 rgb(121, 210, 184, maxColorValue = 255), # 4. bluish green
                 rgb(244, 242, 180, maxColorValue = 255), # 5. yellow
                 rgb(101, 161, 210, maxColorValue = 255), # 6. blue
                 rgb(237, 166, 132, maxColorValue = 255), # 7. vermillion
                 rgb(232, 178, 210, maxColorValue = 255)) # 8. reddish purple

cb_cols_lit <- c(rgb(229, 229, 229, maxColorValue = 255), # 1. black
                 rgb(252, 245, 229, maxColorValue = 255), # 2. orange
                 rgb(232, 245, 254, maxColorValue = 255), # 3. sky blue
                 rgb(224, 242, 237, maxColorValue = 255), # 4. bluish green
                 rgb(252, 252, 216, maxColorValue = 255), # 5. yellow
                 rgb(229, 235, 247, maxColorValue = 255), # 6. blue
                 rgb(251, 234, 224, maxColorValue = 255), # 7. vermillion
                 rgb(255, 233, 245, maxColorValue = 255)) # 8. reddish purple

cb_cols_medlit <- c(rgb(216, 216, 216, maxColorValue = 255), # 1. black
                    rgb(246, 225, 183, maxColorValue = 255), # 2. orange
                    rgb(201, 231, 251, maxColorValue = 255), # 3. sky blue
                    rgb(175, 228, 213, maxColorValue = 255), # 4. bluish green
                    rgb(249, 248, 210, maxColorValue = 255), # 5. yellow
                    rgb(163, 199, 228, maxColorValue = 255), # 6. blue
                    rgb(244, 202, 181, maxColorValue = 255), # 7. vermillion
                    rgb(242, 209, 228, maxColorValue = 255)) # 8. reddish purple
