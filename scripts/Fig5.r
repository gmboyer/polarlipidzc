# load libraries
suppressMessages({
  library(reshape2)
  library(ggplot2)
  library(scales)
})

# load current IPL_master(s)
IPL_master <- readRDS("IPL_master.rds")

mipl <- IPL_master[["mergedIPL 1"]]

# Calculate fractions of linkages
ether <- mipl[["y_frac_ether"]] + mipl[["y_frac_GDGT"]]
ester <- mipl[["y_frac_ester"]]
amide <- mipl[["y_frac_amide"]]
nonlinkage <- mipl[["y_frac_nonlinkage"]]

all_data <- data.frame(ether = ether, ester = ester, amide = amide, CC = nonlinkage)

all_group <- c("Octopus OF2", "Octopus OF1", "Empress OF1",
               "Empress OF2", "Empress OF3", "Empress OF4",
               "Empress OF5", "Mound OF2", "Bison OF1", "Bison OF2",
               "Bison OF3", "Bison OF4", "Bison OF5", "Bison OF6",
               "Mound OF1", "Mound OF3", "Mound OF4", "Mound OF5")

OF <- c("OS2", "OS1", "EP1",
        "EP2", "EP3", "EP4",
        "EP5", "MS2", "BP1", "BP2",
        "BP3", "BP4", "BP5", "BP6",
        "MS1", "MS3", "MS4", "MS5")

spring <- c("Octopus", "Octopus", "Empress",
            "Empress", "Empress", "Empress",
            "Empress", "Mound", "Bison", "Bison",
            "Bison", "Bison", "Bison", "Bison",
            "Mound", "Mound", "Mound", "Mound")

rownames(all_data) <- all_group
all_data <- cbind(all_data, OF, spring)


bison_data <- all_data[seq(9, 14), ]
mound_data <- all_data[c(15, 8, 16, 17, 18), ]
empress_data <- all_data[seq(3, 7), ]
octopus_data <- all_data[c(2, 1), ]


all_data <- rbind(bison_data, mound_data, empress_data, octopus_data)

df <- suppressMessages(melt(all_data))

colnames(df) <- c('Site', 'Spring', 'Linkage', 'Fraction')

# required to re-order sites from OF1 to OF6 instead of in reverse order
df$Site <- factor(df$Site, levels = levels(as.factor(df$Site))[18:1])

# required to get facet_grid to plot springs in the desired order
df$Spring_f <- factor(df$Spring, levels = c("Bison", "Mound", "Empress", "Octopus"))

# colors (ether, ester, amide, cc), references 'scripts//pub_colors.r'
my_col <- cb_cols_drk[c(8, 5, 2, 3)]



p <- (ggplot(df, aes(x = Site, y = Fraction, fill = Linkage))
    + geom_bar(colour = NA, position = "fill", stat = "identity")
    + scale_fill_manual(labels = c("Ether", "Ester", "Amide", "C-C"), values = my_col)
    # or:
    # geom_bar(position = position_fill(), stat = "identity")
    + scale_y_continuous(labels = percent_format())
    + labs(title = NULL, x = NULL, y = "Percent Abundance")
    + theme_minimal(base_size = 15)
    + theme(panel.grid.major = element_blank(),
      panel.grid.minor = element_blank(),
      axis.text.x = element_text(color = "black"),
      axis.text.y = element_text(color = "black"),
      axis.ticks.x = element_line(color = "black"),
      plot.title = element_text(color = "black", face = "bold", hjust = 0.5),
      strip.placement = "outside", # position spring names outside of site names
      strip.text.y = element_text(size = 15, colour = "black")
      )
    + coord_flip()
    # + scale_x_discrete(limits = rev(levels(df$Site)))
    + facet_grid(Spring_f ~ ., scales = "free_y", space = "free_y", switch = "y")
    )


# create a pdf of figure
pdf(file="Fig5.pdf", width=7, height=8)
print(p)
dev.off()

# setEPS()
# postscript("Fig5.eps", width=7, height=8)
# print(p)
# dev.off()

print("Created Fig5.pdf")