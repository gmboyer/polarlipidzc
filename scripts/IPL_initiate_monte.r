# load current IPL_master
IPL_master <- readRDS("IPL_master.rds")

IPLworkbook_directory <- "data//"

monte_iter <- 999 # number of iterations
seed <- 190424   # random seed
peak_vary <- 0.3 # random multiplier for IPL peak areas
RF_vary <- 100   # random multiplier for IPL response factors

IPL_master_monte <- IPL_monte(IPL_master, IPLworkbook_directory, seed,
  monte_iter=monte_iter, peak_vary=peak_vary, RF_vary=RF_vary)

# save results
saveRDS(IPL_master_monte, "IPL_master_monte_area_RF.rds")

print("Created IPL_master_monte_area_RF.rds")