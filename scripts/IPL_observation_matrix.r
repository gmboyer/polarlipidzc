# This code creates a .csv containing names, masses, and observation details

# load current IPL_master(s)
IPL_master <- readRDS("IPL_master.rds")

mas <- IPL_master[[length(IPL_master)]]

IPL_names <- rownames(mas[["IPLareasRFmol"]])
IPL_names <- sub("TM-Lysine 2", "TM-Lysine2", IPL_names) # rename "TM-Lysine 2" to "TM-Lysine2" to avoid regex problems in next step.
IPL_names <- sub("OH-OL", "OL-1OH", IPL_names) # specify "OH-OL" as "OL-1OH"
IPL_names <- sub("PI_DAG", "PI-DAG", IPL_names) # correct underscore
IPL_names <- sub("SPG", "DPG", IPL_names) # correct typo
IPL_names <- sub("NAcG-G-DAG", "2G-NAcG-G-DAG", IPL_names)
IPL_names <- sub("3G-2G-NAcG-G-DAG", "3G-NAcG-G-DAG", IPL_names) #correct error introduced by previous sub()
IPL_names <- sub("NAcG-G-DEG", "2G-NAcG-G-DEG", IPL_names)
IPL_names <- sub("3G-2G-NAcG-G-DEG", "3G-NAcG-G-DEG", IPL_names) #correct error introduced by previous sub()
IPL_names <- sub("1Gly", "1G", IPL_names)
IPL_names <- sub("2Gly", "2G", IPL_names)
IPL_names <- sub("3Gly", "3G", IPL_names)

# specify diacylglycerol for certain headgroups
IPL_names <- sub("PME", "PME-DAG", IPL_names)
IPL_names <- sub("PE$", "PE-DAG", IPL_names) # ignores PE-cer
IPL_names <- sub("PC", "PC-DAG", IPL_names)
IPL_names <- sub("BL", "BL-DAG", IPL_names)

IPL_names <- sub("C.* ", "", IPL_names) # trim off chain information from name

max_across_samples <- apply(mas[["IPLareasRFmol"]], MARGIN = 1, max)

data <- data.frame(names = IPL_names, nC = mas[["nC_raw"]], nUnsat = mas[["nUnsat_raw"]], mass = mas[["masses"]])

data_areas <- cbind(data, mas[["IPLareasRFmol"]])

# reorder samples to: Bison, Mound, Empress, Octopus
data_areas <- data_areas[, c(1, 2, 3, 4, 13, 14, 15, 16, 17, 18, 19, 12, 20, 21, 22, 7, 8, 9, 10, 11, 6, 5)]

# delete IPL names as rownames (not needed)
rownames(data_areas) <- c()

# specify adduct for each IPL
NH4 <- c("1G-GDGT-0", "1G-GDGT-1", "1G-GDGT-2", "1G-GDGT-3", "1G-GDGT-4", "1G-GDGT-5",
         "1G-AEG", "1G-DAG", "1G-DEG",
         "2G-GDGT-0", "2G-GDGT-1", "2G-GDGT-2", "2G-GDGT-3", "2G-GDGT-4", "2G-GDGT-5",
         "2G-P-DEG", "2G-P-GDGT-0", "2G-P-GDGT-1", "2G-P-GDGT-2", "2G-P-GDGT-3", "2G-P-GDGT-4", "2G-P-GDGT-5",
         "2G-AEG", "2G-DAG", "2G-DEG", "3G-GDGT-0", "3G-GDGT-1", "3G-GDGT-2", "3G-GDGT-3", "3G-GDGT-4", "3G-GDGT-5",
         "3G-P-GDGT-0", "3G-P-GDGT-1", "3G-P-GDGT-2", "3G-P-GDGT-3", "3G-P-GDGT-4", "3G-P-GDGT-5",
         "3G-DAG", "3G-DEG", "4G-GDGT-0", "4G-GDGT-1", "4G-GDGT-2", "4G-GDGT-3", "4G-GDGT-4", "4G-GDGT-5",
         "DPG", "G-GA-DAG", "GA-DAG", "Lyso-1G", "Lyso-1G-ether", "Lyso-2G", "Lyso-SQ",
         "PG-DAG", "PI-AEG", "PI-DAG", "PI-DEG", "SQ-DAG")

H   <- c("1G-P-GDGT-0 [H+]", "1G-P-GDGT-1 [H+]", "1G-P-GDGT-2 [H+]", "1G-P-GDGT-3 [H+]", "1G-P-GDGT-4 [H+]", "1G-P-GDGT-5 [H+]",
         "1G-Cer", "223-DAG", "2G-P-AR [H+]", "3G-NAcG-G-DEG", "APT-AEG", "APT-DAG", "APT-DEG",
         "G-MeNG-G-P-AR [H+]", "G-NG-DAG", "G-NG-DEG", "G-NG-G-P-AR [H+]", "MeNG-G-P-AR [H+]",
         "2G-NAcG-G-DAG", "2G-NAcG-G-DEG", "NAcG-P-DAG", "NAcG-P-DEG", "NG-G-P-AR [H+]",
         "NG-GA-AEG", "NG-GA-DAG", "NG-GA-DEG", "OL", "OL-1OH", "PDME-DAG", "PE-AR",
         "PE-Cer", "PE-DAG", "PI-Cer", "PME-DAG", "PS-DAG")

none <- c("BL-DAG", "Lyso-TM-OL", "PC-DAG", "TM-Lysine", "TM-Lysine2", "TM-OL", "TM-OL-1OH")


data_areas[, "adduct"] <- character()

for (IPL in NH4){
  data_areas[data_areas[, "names"] == IPL, "adduct"] <- "NH4+"
}
for (IPL in H){
  data_areas[data_areas[, "names"] == IPL, "adduct"] <- "H+"
}
for (IPL in none){
  data_areas[data_areas[, "names"] == IPL, "adduct"] <- "none"
}

data_areas <- data_areas[, c(1:3, 23, 4:22)]

# write .csvs of data
write.csv(data_areas, "IPL_abund.csv")

print("Created IPL_abund.csv")