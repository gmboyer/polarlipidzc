
suppressMessages({
  library(tidyverse)
  library(reshape2)
  library(ggplot2)
  library(ggthemes)
  library(quantreg)
  library(dplyr)
  library(ggpubr)
})

# load current IPL_master(s)
IPL_master <- readRDS("IPL_master.rds")
IPL_master_no_GDGTs <- readRDS("IPL_master_no_GDGTs.rds")

this_IPL <- IPL_master[["mergedIPL 1"]]
this_IPL_no_GDGTs <- IPL_master_no_GDGTs[["mergedIPL 1"]]

# specify x-axis values
temperature <- c(59.8, 85.4, 82.1,
                70.5, 60.7, 51.6,
                38.1, 77.3, 89.0,
                80.2, 73.7, 62.7,
                37.2, 27.6, 91.0,
                64.8, 53.0, 35.1)

O2 <- c(1.03E-4, 1.56E-5, 1.19E-5,
        2.47E-5, 3.75E-5, 4.06E-5,
        1.06E-4, 6.88E-5, 6.25E-6,
        2.19E-5, 3.44E-5, 7.19E-5,
        1.78E-4, 1.03E-4, 1.19E-5,
        4.38E-5, 1.13E-4, 2.16E-4)

logO2 <- log10(O2)

df <- data.frame(cbind(tempC=temperature,
                 logO2=logO2,
                 nC=this_IPL[["IPL_nCave"]],
                 nC_no_GDGTs=this_IPL_no_GDGTs[["IPL_nCave"]],
                 nUnsat=this_IPL[["IPL_nUnsatave"]],
                 nUnsat_no_GDGTs=this_IPL_no_GDGTs[["IPL_nUnsatave"]],
                 nHexRing=this_IPL[["IPL_nHexRingave"]],
                 nPentRing=this_IPL[["IPL_nPentRingave"]]))

df <- cbind(df, xGDGT=this_IPL[["y_frac_GDGT"]])
df <- cbind(df, nOH=this_IPL[["y_frac_hydroxylated"]])

get_spring <- function(str){
  str <- tolower(str)
  ifelse(grepl("bison", str), "Bison Pool",
    ifelse(grepl("mound", str), "Mound Spring",
    ifelse(grepl("empress", str), "Empress Pool", "Octopus Spring")))
}

df <- df %>%
  rownames_to_column('sample') %>%
  mutate(nRing=nHexRing+nPentRing) %>%
  mutate(RingsPerGDGT=(nRing/xGDGT)*4) %>%
  mutate(site=get_spring(sample))

# logO2_label <- expression(paste("log[O"[2]*", mol"%.%"kg"^{-1}*"]"))
logO2_label <- expression(paste("log[O"[2]*", molal]"))

# nC
nC_breaks <- seq(16, 21, 1)
nC_limits <- c(16, 21)
p1 <- (ggplot(df)
        + geom_point(aes(y=nC_no_GDGTs, x=tempC, shape=site), col="black", fill="white", size=3, show.legend=TRUE)
        + geom_point(aes(y=nC, x=tempC, shape=site), col="black", fill="darkgray", size=3, show.legend=TRUE)
        + scale_y_continuous(breaks=nC_breaks, limits=nC_limits)
        + labs(x=expression(paste("Temperature, ", degree, "C", sep = "")), y="nC")
      )

p2 <- (ggplot(df)
        + geom_point(aes(y=nC_no_GDGTs, x=logO2, shape=site), col="black", fill="white", size=3, show.legend=TRUE)
        + geom_point(aes(y=nC, x=logO2, shape=site), col="black", fill="darkgray", size=3, show.legend=TRUE)
        + scale_y_continuous(breaks=nC_breaks, limits=nC_limits)
        + labs(x=logO2_label, y="nC")
      )

# nUnsat
nUnsat_breaks <- seq(0, 1, 0.2)
nUnsat_limits <- c(0, 1.0)
p3 <- (ggplot(df)
        + geom_point(aes(y=nUnsat_no_GDGTs, x=tempC, shape=site), col="black", fill="white", size=3, show.legend=TRUE)
        + geom_point(aes(y=nUnsat, x=tempC, shape=site), col="black", fill="darkgray", size=3, show.legend=TRUE)
        + scale_y_continuous(breaks=nUnsat_breaks, limits=nUnsat_limits)
        + labs(x=expression(paste("Temperature, ", degree, "C", sep = "")), y="nUnsat")
      )

p4 <- (ggplot(df)
        + geom_point(aes(y=nUnsat_no_GDGTs, x=logO2, shape=site), col="black", fill="white", size=3, show.legend=TRUE)
        + geom_point(aes(y=nUnsat, x=logO2, shape=site), col="black", fill="darkgray", size=3, show.legend=TRUE)
        + scale_y_continuous(breaks=nUnsat_breaks, limits=nUnsat_limits)
        + labs(x=logO2_label, y="nUnsat")
      )

# Rings per GDGT
ring_breaks <- seq(1, 4, 0.5)
ring_limits <- c(1, 4)
p5 <- (ggplot(df)
        + geom_point(aes(y=RingsPerGDGT, x=tempC, shape=site), col="black", fill="darkgray", size=3, show.legend=TRUE)
        + scale_y_continuous(breaks=ring_breaks, limits=ring_limits)
        + labs(x=expression(paste("Temperature, ", degree, "C", sep = "")), y="Rings per GDGT")
      )

p6 <- (ggplot(df)
        + geom_point(aes(y=RingsPerGDGT, x=logO2, shape=site), col="black", fill="darkgray", size=3, show.legend=TRUE)
        + scale_y_continuous(breaks=ring_breaks, limits=ring_limits)
        + labs(x=logO2_label, y="Rings per GDGT")
      )

# nOH
nOH_breaks <- seq(0, .014, 0.002)
nOH_limits <- c(0, .014)
p7 <- (ggplot(df)
        + geom_point(aes(y=nOH, x=tempC, shape=site), col="black", fill="darkgray", size=3, show.legend=TRUE)
        + scale_y_continuous(breaks=nOH_breaks, limits=nOH_limits)
        + labs(x=expression(paste("Temperature, ", degree, "C", sep = "")), y="nOH")
      )

p8 <- (ggplot(df)
        + geom_point(aes(y=nOH, x=logO2, shape=site), col="black", fill="darkgray", size=3, show.legend=TRUE)
        + scale_y_continuous(breaks=nOH_breaks, limits=nOH_limits)
        + labs(x=logO2_label, y="nOH")
      )


plot_symbols <- scale_shape_manual(values=c("Bison Pool"=21, "Mound Spring"=24, "Empress Pool"=22, "Octopus Spring"=23))

plot_tempC_x_axis <- scale_x_continuous(breaks = c(20, 30, 40, 50, 60, 70, 80, 90, 100), limits = c(20, 100))
plot_logO2_x_axis <- scale_x_continuous(breaks=c(-5.5, -5.0, -4.5, -4.0, -3.5), limits=c(-5.5, -3.5))

plot_theme <- theme(panel.grid.major = element_blank(),
                    panel.grid.minor = element_blank(),
                    panel.background = element_blank(),
                    axis.text.x = element_text(color="black"),
                    axis.text.y = element_text(color="black"),
                    axis.ticks.x = element_line(color="black"),
                    axis.ticks.y = element_line(color="black"),
                    axis.line.x = element_line(color="black"),
                    axis.line.y = element_line(color="black"),
                    legend.title = element_blank(),
                    legend.key = element_blank())
legend_guide <- guides(shape=guide_legend(
                       keywidth=0.3,
                       keyheight=0.3,
                       default.unit="inch"))

p1 <- p1 + plot_tempC_x_axis + plot_symbols + plot_theme + legend_guide
p2 <- p2 + plot_logO2_x_axis + plot_symbols + plot_theme + legend_guide
p3 <- p3 + plot_tempC_x_axis + plot_symbols + plot_theme + legend_guide
p4 <- p4 + plot_logO2_x_axis + plot_symbols + plot_theme + legend_guide
p5 <- p5 + plot_tempC_x_axis + plot_symbols + plot_theme + legend_guide
p6 <- p6 + plot_logO2_x_axis + plot_symbols + plot_theme + legend_guide
p7 <- p7 + plot_tempC_x_axis + plot_symbols + plot_theme + legend_guide
p8 <- p8 + plot_logO2_x_axis + plot_symbols + plot_theme + legend_guide

p <- ggarrange(p1, p2, p3, p4, p5, p6, ncol=2, nrow=3, common.legend=TRUE, legend="bottom", labels="AUTO")


# export plot
pdf(file=paste("Fig6.pdf"), width=6, height=8)
print(p)
dev.off()

# setEPS()
# postscript("Fig6.eps", width=6, height=8)
# print(p)
# dev.off()

# jpeg('Fig6.jpg', width=6, height=8, units="in", res=300)
# print(p)
# dev.off()

print("Created Fig6.pdf")