{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Supplementary code\n",
    "\n",
    "Corresponding Author: Grayson Boyer (gmboyer@asu.edu)\n",
    "\n",
    "The purpose of this Jupyter notebook is to organize and execute the R scripts used to process analytical intact polar lipid (IPL) data contained in data/IPL_data.xlsx, calculate $Z_C$ of IPLs and their component parts, perform a monte carlo simulation of uncertainty, and recreate figures 4 though 7.\n",
    "\n",
    "**Instructions:** Ensure you can run this notebook with an R kernel. R version 3.5.3 was used at the time this notebook was created. With an R kernel selected, install the R packages used in these scripts (see below). Once the necessary packages are installed, the notebook can be run by selecting the 'Kernel' tab and then choosing 'Restart and Run All'. The notebook should then be run from top to bottom. It may take a few minutes for the notebook to finish. If you encounter errors when running this notebook, please consult the 'Troubleshooting' section at the bottom of the notebook.\n",
    "\n",
    "**Input:** This notebook references R code located in the 'scripts' folder. IPL data are located in the 'IPL_data.xlsx' Excel workbook in the 'data' folder.\n",
    "\n",
    "**Output:** This notebook generates the following files in the same directory as this notebook:\n",
    "\n",
    "*R objects*\n",
    "- IPL_master.rds\n",
    "- IPL_master_no_GDGTs.rds\n",
    "- IPL_master_monte_area_RF.rds\n",
    "\n",
    "*Tables*\n",
    "- IPL_abund.csv (a table of IPL names, chain lengths, chain unsaturations, adducts, masses, and abundances after response factor correction)\n",
    "\n",
    "*PDF figures*\n",
    "- Fig4.pdf\n",
    "- Fig5.pdf\n",
    "- Fig6.pdf\n",
    "- Fig7.pdf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Installing packages:** Use the cell below to install the R packages used in this notebook. Each package installation might also install dependent packages. Package versions used at the time this notebook was created are indicated to the right of each line. Newer versions might cause errors when running this notebook. The versions of all packages and dependencies are shown in the Troubleshooting section. Remove the # symbol in front of a line to install a given package when the cell is run. These packages only need to be installed the first time this notebook is run. To avoid having to re-install the packages every time the notebook is run, include the # symbol before each line."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# install.packages('readxl') # v 1.3.1\n",
    "# install.packages('scales') # v 1.0.0\n",
    "# install.packages('CHNOSZ') # v 1.3.1\n",
    "# install.packages('msir') # v 1.3.2\n",
    "# install.packages('tidyverse') # v 1.2.1\n",
    "# install.packages('ggthemes') # v 4.1.1\n",
    "# install.packages('quantreg') # v 5.38\n",
    "# install.packages('ggpubr') # v 0.2\n",
    "# install.packages('reshape') # v 0.8.8\n",
    "# install.packages('reshape2') # v 1.4.3\n",
    "# install.packages('ggplot2') # v 3.1.1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Process IPL data and create plots"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This cell loads R libraries and custom functions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Warning message:\n",
      "\"package 'readxl' was built under R version 4.0.2\"\n",
      "Warning message:\n",
      "\"package 'CHNOSZ' was built under R version 4.0.2\"\n"
     ]
    }
   ],
   "source": [
    "source(\"scripts//IPL_functions.r\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Process IPLs and calculate $Z_C$. This may take a minute. Required for figures 4 through 7."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "scrolled": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] \"Creating IPL_master.rds\"\n",
      "   user  system elapsed \n",
      "  24.22    1.58   26.12 \n",
      "[1] \"IPL_master.rds created.\"\n",
      "[1] \"Creating IPL_master_no_GDGTs.rds\"\n",
      "   user  system elapsed \n",
      "  29.44    1.26   34.28 \n",
      "[1] \"IPL_master_no_GDGTs.rds created.\"\n",
      "[1] \"Creating IPL_master_GDGTs.rds\"\n",
      "   user  system elapsed \n",
      "   1.25    0.39    1.90 \n",
      "[1] \"IPL_master_GDGTs.rds created.\"\n"
     ]
    }
   ],
   "source": [
    "source(\"scripts//IPL_initiate_ZC_calcs.r\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Monte Carlo simulation of uncertainty (can take a few minutes for 999 iterations!). Required for Figure 7."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "   user  system elapsed \n",
      " 191.31    0.24  200.43 \n",
      "[1] \"Created IPL_master_monte_area_RF.rds\"\n"
     ]
    }
   ],
   "source": [
    "source(\"scripts//IPL_initiate_monte.r\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Generate a table of observed IPLs. Exports a table called IPL_abund.csv in the same directory as this Jupyter notebook. Required for Figure 4."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] \"Created IPL_abund.csv\"\n"
     ]
    }
   ],
   "source": [
    "source(\"scripts//IPL_observation_matrix.r\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Recreate figures 4 through 7. Figures should be generated in the same directory as the Jupyter notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Warning message:\n",
      "\"package 'reshape' was built under R version 4.0.2\"\n",
      "Warning message:\n",
      "\"package 'ggplot2' was built under R version 4.0.2\"\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] \"Created Fig4.pdf\"\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Warning message:\n",
      "\"package 'reshape2' was built under R version 4.0.2\"\n",
      "Warning message:\n",
      "\"package 'scales' was built under R version 4.0.2\"\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] \"Created Fig5.pdf\"\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Warning message:\n",
      "\"package 'tidyverse' was built under R version 4.0.2\"\n",
      "Warning message:\n",
      "\"package 'tibble' was built under R version 4.0.2\"\n",
      "Warning message:\n",
      "\"package 'tidyr' was built under R version 4.0.2\"\n",
      "Warning message:\n",
      "\"package 'readr' was built under R version 4.0.2\"\n",
      "Warning message:\n",
      "\"package 'purrr' was built under R version 4.0.2\"\n",
      "Warning message:\n",
      "\"package 'dplyr' was built under R version 4.0.2\"\n",
      "Warning message:\n",
      "\"package 'stringr' was built under R version 4.0.2\"\n",
      "Warning message:\n",
      "\"package 'forcats' was built under R version 4.0.2\"\n",
      "Warning message:\n",
      "\"package 'ggthemes' was built under R version 4.0.2\"\n",
      "Warning message:\n",
      "\"package 'quantreg' was built under R version 4.0.2\"\n",
      "Warning message:\n",
      "\"package 'ggpubr' was built under R version 4.0.2\"\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] \"Created Fig6.pdf\"\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Warning message:\n",
      "\"package 'msir' was built under R version 4.0.2\"\n"
     ]
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] \"Created Fig7.pdf\"\n"
     ]
    }
   ],
   "source": [
    "source(\"scripts//pub_colors.r\")\n",
    "source(\"scripts//Fig4.r\")\n",
    "source(\"scripts//Fig5.r\")\n",
    "source(\"scripts//Fig6.r\")\n",
    "source(\"scripts//Fig7.r\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Troubleshooting\n",
    "\n",
    "Updates to R or its packages can break code. This notebook's code was run using R version 3.5.3. You can check the versions of R packages currently loaded using sessionInfo(), given in the cell below. When you run the cell with sessionInfo(), do the \"other attached packages\" in your list correspond to the versions of packages listed below?\n",
    "\n",
    "- msir 1.3.2\n",
    "- ggpubr 0.4.0\n",
    "- quantreg 5.61\n",
    "- SparseM 1.78\n",
    "- ggthemes 4.2.1\n",
    "- forcats 0.5.0\n",
    "- stringr 1.4.0\n",
    "- dplyr 1.0.2\n",
    "- purrr 0.3.4\n",
    "- readr 1.3.1\n",
    "- tidyr 1.1.1\n",
    "- tibble 3.0.3\n",
    "- tidyverse 1.3.0\n",
    "- scales 1.1.1\n",
    "- reshape2 1.4.4\n",
    "- ggplot2 3.3.2\n",
    "- reshape 0.8.8\n",
    "- CHNOSZ 1.3.6\n",
    "- readxl 1.3.1\n",
    "\n",
    "If not, you may have to re-install one or more packages while specifying a specific version. For instance, CHNOSZ version 1.3.6 can be installed from the CRAN project archive by pasting and running the following lines in a new code cell:\n",
    "\n",
    "`\n",
    "url <- \"https://cran.r-project.org/src/contrib/Archive/CHNOSZ/CHNOSZ_1.3.6.tar.gz\"\n",
    "install.packages(url, repos=NULL, type=\"source\")\n",
    "`\n",
    "\n",
    "After installing a library this way, you can check that its installation was successful by running library() and sessionInfo() in a code cell. For instance:\n",
    "\n",
    "`\n",
    "library(CHNOSZ)\n",
    "sessionInfo()\n",
    "`\n",
    "\n",
    "In this case, CHNOSZ_1.3.6 should appear in the output under \"other attached packages\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "R version 4.0.1 (2020-06-06)\n",
       "Platform: x86_64-w64-mingw32/x64 (64-bit)\n",
       "Running under: Windows 10 x64 (build 18363)\n",
       "\n",
       "Matrix products: default\n",
       "\n",
       "locale:\n",
       "[1] LC_COLLATE=English_United States.1252 \n",
       "[2] LC_CTYPE=English_United States.1252   \n",
       "[3] LC_MONETARY=English_United States.1252\n",
       "[4] LC_NUMERIC=C                          \n",
       "[5] LC_TIME=English_United States.1252    \n",
       "\n",
       "attached base packages:\n",
       "[1] stats     graphics  grDevices utils     datasets  methods   base     \n",
       "\n",
       "other attached packages:\n",
       " [1] msir_1.3.2      ggpubr_0.4.0    quantreg_5.61   SparseM_1.78   \n",
       " [5] ggthemes_4.2.0  forcats_0.5.0   stringr_1.4.0   dplyr_1.0.2    \n",
       " [9] purrr_0.3.4     readr_1.3.1     tidyr_1.1.1     tibble_3.0.3   \n",
       "[13] tidyverse_1.3.0 scales_1.1.1    reshape2_1.4.4  ggplot2_3.3.2  \n",
       "[17] reshape_0.8.8   CHNOSZ_1.3.6    readxl_1.3.1   \n",
       "\n",
       "loaded via a namespace (and not attached):\n",
       " [1] nlme_3.1-148       matrixStats_0.56.0 fs_1.4.1           lubridate_1.7.9   \n",
       " [5] httr_1.4.1         repr_1.1.0         tools_4.0.1        backports_1.1.7   \n",
       " [9] R6_2.4.1           DBI_1.1.0          colorspace_1.4-1   withr_2.2.0       \n",
       "[13] tidyselect_1.1.0   gridExtra_2.3      curl_4.3           compiler_4.0.1    \n",
       "[17] cli_2.0.2          rvest_0.3.5        xml2_1.3.2         labeling_0.3      \n",
       "[21] pbdZMQ_0.3-3       digest_0.6.25      foreign_0.8-80     rio_0.5.16        \n",
       "[25] base64enc_0.1-3    pkgconfig_2.0.3    htmltools_0.4.0    dbplyr_1.4.4      \n",
       "[29] rlang_0.4.7        rstudioapi_0.11    farver_2.0.3       generics_0.0.2    \n",
       "[33] jsonlite_1.6.1     mclust_5.4.6       zip_2.0.4          car_3.0-8         \n",
       "[37] magrittr_1.5       Matrix_1.2-18      Rcpp_1.0.4.6       IRkernel_1.1      \n",
       "[41] munsell_0.5.0      fansi_0.4.1        abind_1.4-5        lifecycle_0.2.0   \n",
       "[45] stringi_1.4.6      carData_3.0-4      plyr_1.8.6         grid_4.0.1        \n",
       "[49] blob_1.2.1         crayon_1.3.4       lattice_0.20-41    IRdisplay_0.7.0   \n",
       "[53] haven_2.3.1        cowplot_1.0.0      hms_0.5.3          pillar_1.4.4      \n",
       "[57] uuid_0.1-4         ggsignif_0.6.0     reprex_0.3.0       glue_1.4.1        \n",
       "[61] evaluate_0.14      data.table_1.12.8  modelr_0.1.8       vctrs_0.3.2       \n",
       "[65] MatrixModels_0.4-1 cellranger_1.1.0   gtable_0.3.0       assertthat_0.2.1  \n",
       "[69] openxlsx_4.1.5     broom_0.5.6        rstatix_0.6.0      conquer_1.0.1     \n",
       "[73] ellipsis_0.3.1    "
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "sessionInfo()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "R",
   "language": "R",
   "name": "ir"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "4.0.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
